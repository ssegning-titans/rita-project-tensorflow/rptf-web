import {useSingleQrCode} from "../service/swr-axios";
import {useParams} from "react-router-dom";
import QRCode from 'react-qr-code';
import {useMemo} from "react";

export function SingleQrcode() {
    const {id} = useParams();
    const {data, isLoading, error} = useSingleQrCode(id!);
    const codeStr = useMemo(() => {
        if (data) {
            // Because we can it with "id"
            (data as any).id = data?._id;
            return JSON.stringify(data);
        }
        return '';
    }, [data]);

    if (error) {
        return (
            <div>
                <h4 className='text-danger'>Error loading QrCode</h4>
            </div>
        )
    }

    if (isLoading || !data) {
        return (
            <div className="spinner-border" role="status">
                <span className="visually-hidden">Loading...</span>
            </div>
        )
    }

    return (
        <div>
            <h4 className='mb-3'>{data.name}</h4>

            <QRCode value={codeStr}/>
        </div>
    );
}