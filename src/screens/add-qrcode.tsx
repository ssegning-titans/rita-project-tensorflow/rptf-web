import {Field, Form, FormikProps, withFormik} from "formik";
import * as Yup from "yup";
import {useCallback} from "react";
import {useCreateQrCode} from "../service/swr-axios";
import {QrCode} from "../types/qr-code";
import {FeatureEnum} from "../types/feature";
import {useNavigate} from "react-router-dom";

const schemaYup = Yup
    .object({
        name: Yup.string()
            .max(255, 'Must be 15 characters or less')
            .required('Required'),
        features: Yup.array()
            .min(1, 'Minimum 1 and maximum 3 values should be picked')
            .max(3, 'Minimum 1 and maximum 3 values should be picked')
            .required('Required'),
    });

interface FormValues {
    name: string;
    features: string[];
}

interface AddFormProps {
    onSubmit: (values: FormValues) => Promise<void>;
    name?: string;
}

function InnerForm(props: FormikProps<FormValues>) {
    const {touched, errors, isSubmitting, submitCount} = props;
    return (
        <Form className={`${submitCount > 0 ? 'was-validated' : ''}`}>
            <div className="mb-3">
                <label htmlFor="nameQrCode" className="form-label">Name</label>
                <Field className="form-control" id="nameQrCode" type="text" name="name" required/>
                {touched.name && errors.name && (
                    <div className="invalid-feedback">
                        {errors.name}
                    </div>
                )}
            </div>

            <div className="mb-3">
                <label htmlFor="selectQrCode" className="form-label">Features</label>
                <Field className="form-control form-select" id="selectQrCode" as="select" name="features"
                       multiple required>
                    <option value="TIME">Time</option>
                    <option value="OBJ">Object</option>
                    <option value="COLOR">Color</option>
                </Field>
                {touched.features && errors.features && (
                    <div className="invalid-feedback">
                        {errors.features}
                    </div>
                )}
            </div>

            <button className='btn btn-outline-primary' type="submit" disabled={isSubmitting}>
                Submit
            </button>
        </Form>
    );
}

const AddForm = withFormik<AddFormProps, FormValues>({
    // Transform outer props into form values
    mapPropsToValues: (props) => ({
        name: props.name ?? '',
        features: [],
    }),
    validationSchema: schemaYup,
    handleSubmit: async (values, {setSubmitting, props}) => {
        try {
            await props.onSubmit(values);
        } finally {
            setSubmitting(false);
        }
    },
})(InnerForm);

export function AddQrcode() {
    const [mutate] = useCreateQrCode();
    const navigate = useNavigate();

    const onSubmit = useCallback(async (value: FormValues) => {
        const body: QrCode = {
            name: value.name,
            features: value.features.map((name) => ({
                name: name as FeatureEnum,
            })),
        };
        const result = await mutate(body);
        navigate(`/qr-codes/${result._id}`);
    }, [mutate, navigate]);

    return (
        <div>
            <h4 className='mb-3'>Add page</h4>

            <AddForm onSubmit={onSubmit}/>
        </div>
    );
}