import {useResults} from "../service/swr-axios";
import {ResultListItem} from "../components/result-list-item";

export function AllResult() {
    const {data, isLoading, error} = useResults();

    if (error) {
        return (
            <div>
                <h4 className='text-danger'>Error loading Results</h4>
            </div>
        )
    }

    if (isLoading || !data) {
        return (
            <div className="spinner-border" role="status">
                <span className="visually-hidden">Loading...</span>
            </div>
        )
    }

    return (
        <div className="accordion">
            {data.map(d => (
                <ResultListItem key={d._id} {...d} />
            ))}
        </div>
    );
}