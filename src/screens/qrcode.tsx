import {Link, Outlet} from "react-router-dom";

export function Qrcode() {
    return (
        <div>
            <h2>QR Code</h2>
            <hr/>

            <div className='mb-3'>
                <Link to='/qr-codes' type="button" className="btn btn-outline-primary">
                    See all
                </Link>
                <Link to='/qr-codes/add' type="button" className="btn btn-outline-primary ms-2">
                    Add QrCode
                </Link>
            </div>

            <Outlet/>
        </div>
    );
}