import * as Yup from "yup";
import {Field, Form, FormikProps, withFormik} from "formik";
import {useFindResults} from "../service/swr-axios";
import {useCallback, useState} from "react";
import {Result} from "../types/result";
import {ResultListItem} from "../components/result-list-item";

export const timeMapping = {
    0: 'afternoon',
    1: 'morning',
    2: 'night',
    3: 'noon',
};

export const colorMapping = {
    0: 'black',
    1: 'blue',
    2: 'green',
    3: 'purple',
    4: 'red',
    5: 'white',
    6: 'yellow',
};

const schemaYup = Yup
    .object({
        feature: Yup.string()
            .max(255, 'Must be 15 characters or less')
            .required('Required'),
        option: Yup.string()
            .required('Required'),
        minProbability: Yup.number()
            // .min(0, 'Minimum 0 and maximum 1 values should be picked')
            // .max(1, 'Minimum 0 and maximum 1 values should be picked')
            .required('Required'),
        limit: Yup.number()
            // .min(0, 'Minimum 0 and maximum 1 values should be picked')
            // .max(1, 'Minimum 0 and maximum 1 values should be picked')
            .required('Required'),
    });

interface FormValues {
    feature: string;
    option: string;
    minProbability: number;
    limit: number;
}

interface SearchFormProps {
    onSubmit: (values: FormValues) => Promise<void>;
}

function InnerForm(props: FormikProps<FormValues>) {
    const {touched, errors, isSubmitting, submitCount, values} = props;
    return (
        <Form className={`${submitCount > 0 ? 'was-validated' : ''}`}>
            <div className="mb-3">
                <label htmlFor="selectQrCode" className="form-label">Feature</label>
                <Field className="form-control form-select" id="selectQrCode" as="select" name="feature" required>
                    <option>Select feature</option>
                    <option value="TIME">Time</option>
                    <option value="OBJ">Object</option>
                    <option value="COLOR">Color</option>
                </Field>
                {touched.feature && errors.feature && (
                    <div className="invalid-feedback">
                        {errors.feature}
                    </div>
                )}
            </div>

            <div className="mb-3">
                <label htmlFor="option" className="form-label">Option</label>
                <Field className="form-control form-select" id="option" as="select"
                       name="option" required>
                    <option>Select option</option>
                    {Object.values(values.feature === 'TIME' ? timeMapping : values.feature === 'COLOR' ? colorMapping : {}).map((v) => (
                        <option key={v} value={v}>{v}</option>
                    ))}
                </Field>
                {touched.option && errors.option && (
                    <div className="invalid-feedback">
                        {errors.option}
                    </div>
                )}
            </div>

            <div className="mb-3">
                <label htmlFor="minProbability" className="form-label">Min Probability</label>
                <Field
                    className="form-control"
                    id="minProbability"
                    type="number"
                    name="minProbability"
                    required
                />
                {touched.minProbability && errors.minProbability && (
                    <div className="invalid-feedback">
                        {errors.minProbability}
                    </div>
                )}
            </div>

            <div className="mb-3">
                <label htmlFor="limit" className="form-label">Limit</label>
                <Field
                    className="form-control"
                    id="limit"
                    type="number"
                    name="limit"
                    required
                    min={0}
                    max={5}
                />
                {touched.limit && errors.limit && (
                    <div className="invalid-feedback">
                        {errors.limit}
                    </div>
                )}
            </div>

            <button className='btn btn-outline-primary' type="submit" disabled={isSubmitting}>
                Search
            </button>
        </Form>
    );
}

const SearchForm = withFormik<SearchFormProps, FormValues>({
    // Transform outer props into form values
    mapPropsToValues: () => ({
        feature: '',
        minProbability: -1,
        option: '',
        limit: 3,
    }),
    validationSchema: schemaYup,
    handleSubmit: async (values, {setSubmitting, props}) => {
        try {
            await props.onSubmit(values);
        } finally {
            setSubmitting(false);
        }
    },
})(InnerForm);

export function SearchResult() {
    const [mutate] = useFindResults();
    const [results, setResults] = useState<Result[]>([]);

    const onSubmit = useCallback(async (value: FormValues) => {
        const body: any = {
            feature: value.feature,
            option: value.option,
            minProbability: value.minProbability,
            limit: 3,
        };
        const result = await mutate(body);
        setResults(result);
    }, [mutate]);

    return (
        <div>
            <h4 className='mb-3'>Search</h4>

            <SearchForm onSubmit={onSubmit}/>

            <div className="accordion my-3">
                {results.map(d => (
                    <ResultListItem key={d._id} {...d} />
                ))}
            </div>
        </div>
    );
}