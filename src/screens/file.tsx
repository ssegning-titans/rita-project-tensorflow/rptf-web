import {Outlet} from "react-router-dom";

export function File() {
    return (
        <div>
            <h2>File</h2>
            <hr/>

            <Outlet/>
        </div>
    );
}