import {Link, Outlet} from "react-router-dom";

export function Result() {
    return (
        <div>
            <h2>Result page</h2>
            <hr/>

            <div className='mb-3'>
                <Link to='/results' type="button" className="btn btn-outline-primary">
                    See all
                </Link>
                <Link to='/results/search' type="button" className="btn btn-outline-primary ms-2">
                    Search
                </Link>
            </div>

            <Outlet />
        </div>
    );
}