import {useQrCode} from "../service/swr-axios";
import {Link} from "react-router-dom";

export function AllQrcode() {
    const {data, isLoading, error} = useQrCode();

    if (error) {
        return (
            <div>
                <h4 className='text-danger'>Error loading QrCodes</h4>
            </div>
        )
    }

    if (isLoading || !data) {
        return (
            <div className="spinner-border" role="status">
                <span className="visually-hidden">Loading...</span>
            </div>
        )
    }

    return (
        <ul className="list-group my-3">
            {data.map(d => (
                <Link to={`/qr-codes/${d._id}`} key={d._id}
                      className="list-group-item list-group-item-action d-flex justify-content-between align-items-center">
                    <span>{d.name}</span>
                    <span>
                        {d.features.map((f) => (
                            <span key={f.name} className="badge text-bg-primary ms-2">{f.name}</span>
                        ))}
                    </span>
                </Link>
            ))}
        </ul>
    );
}