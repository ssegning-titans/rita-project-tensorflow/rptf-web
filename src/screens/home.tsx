import {Link, Outlet, useLocation} from "react-router-dom";
import {useMemo} from "react";

export function Home() {
    const location = useLocation();

    const active = useMemo(() => {
        if (location.pathname.startsWith('/qr-codes')) return 'qr-codes';
        if (location.pathname.startsWith('/results')) return 'results';
        if (location.pathname.startsWith('/files')) return 'files';
        return '';
    }, [location.pathname]);

    return (
        <div>
            <nav className="navbar navbar-dark bg-primary navbar-expand-md text-white sticky-top bg-body-tertiary">
                <div className="container">
                    <Link to='/' className="navbar-brand">RPTF Web</Link>
                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse"
                            data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup"
                            aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
                        <div className="navbar-nav">
                            <Link className={`nav-link ${active === '' && 'active'}`}
                                  to="/">Home</Link>
                            <Link className={`nav-link ${active === 'qr-codes' && 'active'}`}
                                  to="/qr-codes">
                                QR Codes</Link>
                            <Link className={`nav-link ${active === 'results' && 'active'}`}
                                  to="/results">Results</Link>
                            <Link className={`nav-link ${active === 'files' && 'active'}`}
                                  to="/files">Files</Link>
                        </div>
                    </div>
                </div>
            </nav>

            <main className='container pt-4'>
                <Outlet/>
            </main>
        </div>
    );
}