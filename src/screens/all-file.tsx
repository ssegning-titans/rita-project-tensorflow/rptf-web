import {baseUrl, useFiles} from "../service/swr-axios";
import './all-file.scss';

export function AllFile() {
    const {data, isLoading, error} = useFiles();

    if (error) {
        return (
            <div>
                <h4 className='text-danger'>Error loading files</h4>
            </div>
        )
    }

    if (isLoading || !data) {
        return (
            <div className="spinner-border" role="status">
                <span className="visually-hidden">Loading...</span>
            </div>
        )
    }

    return (
        <div id='all-files'>
            <div className='row'>
                {data.map(d => (
                    <div key={d.id} className="col-6 col-sm-4 col-md-3">
                        <img src={`${baseUrl}/api/v1/files/photos/${d.filename}`} className="img-fluid" alt={d.filename}/>
                    </div>
                ))}
            </div>
        </div>
    );
}