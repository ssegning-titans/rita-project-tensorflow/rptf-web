import {baseUrl} from "../service/swr-axios";
import {Result} from "../types/result";
import './result-list-item.scss';

import {Link} from "react-router-dom";
import prettyBytes from 'pretty-bytes';

export function ResultListItem({
                                   fileName,
                                   feature,
                                   fileSize,
                                   _id,
                                   endDate,
                                   startDate,
                                   relatedQrCode,
                                   trackId,
                                   predictions
                               }: Result) {
    const imageUrl = `${baseUrl}/api/v1/files/photos/${fileName}`;
    return (
        <div className="accordion-item">
            <h2 className="accordion-header" id={"headingOne" + _id}>
                <button className="accordion-button" type="button" data-bs-toggle="collapse"
                        data-bs-target={"#collapseOne" + _id} aria-expanded="false" aria-controls={"collapseOne" + _id}>
                    <div className='acc-image me-2' style={{
                        backgroundImage: `url(${imageUrl})`
                    }}></div>

                    <span>Result of extracting feature {feature.name.toLowerCase()} of {fileName}</span>
                </button>
            </h2>

            <div id={"collapseOne" + _id} className="accordion-collapse collapse"
                 aria-labelledby={"headingOne" + _id}
                 data-bs-parent="#accordionExample">
                <div className="accordion-body">
                    <p>This is the result for the job running for the feature <b>{feature.name}</b> on the
                        image <b>{fileName}</b></p>

                    <ul className="list-group my-3">
                        {Object.keys(predictions).map((p) => (
                            <li key={p}
                                className="list-group-item d-flex justify-content-between align-items-center overflow-auto">
                                <span className="badge bg-dark rounded-pill">{p}</span>
                                <span>{predictions[p]}</span>
                            </li>
                        ))}
                    </ul>

                    <ul className="list-group my-3">
                        <li className="list-group-item d-flex justify-content-between align-items-center overflow-auto">
                            <span>Image url:</span>
                            <a className='ms-2 link-info' target='_blank' rel='noreferrer'
                               href={imageUrl}>{`${imageUrl}`}</a>
                        </li>
                        <li className="list-group-item d-flex justify-content-between align-items-center overflow-auto">
                            <span>File Size:</span>
                            <span className='ms-2'>{prettyBytes(fileSize)}</span>
                        </li>
                        <li className="list-group-item d-flex justify-content-between align-items-center overflow-auto">
                            <span>TrackId:</span>
                            <span className='ms-2'>{trackId}</span>
                        </li>
                        {relatedQrCode?.name && (
                            <li className="list-group-item d-flex justify-content-between align-items-center overflow-auto">
                                <span>Qr Code:</span>
                                <Link to={'/qr-codes/' + relatedQrCode._id}
                                      className='ms-2'>{relatedQrCode?.name}</Link>
                            </li>
                        )}
                        <li className="list-group-item d-flex justify-content-between align-items-center overflow-auto">
                            <span>Start date:</span>
                            <span className='ms-2'>{new Date(startDate).toUTCString()}</span>
                        </li>
                        <li className="list-group-item d-flex justify-content-between align-items-center overflow-auto">
                            <span>End date:</span>
                            <span className='ms-2'>{new Date(endDate).toUTCString()}</span>
                        </li>
                    </ul>

                    <div>
                        <span className="badge bg-primary rounded-pill">{feature.name}</span>
                        {relatedQrCode?.name &&
                            <span className="badge bg-info rounded-pill">{relatedQrCode?.name}</span>}
                    </div>
                </div>
            </div>
        </div>
    );
}