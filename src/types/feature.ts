export interface Feature {
    name: FeatureEnum;
}

export enum FeatureEnum {
    TIME = 'TIME',
    OBJ = 'OBJ',
    COLOR = 'COLOR',
}