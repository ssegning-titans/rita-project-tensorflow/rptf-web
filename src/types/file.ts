export interface AppFile {
    filename: string;
    length: number;
    chunkSize: number;
    contentType: string;
    id: string;
    meta: any;
}