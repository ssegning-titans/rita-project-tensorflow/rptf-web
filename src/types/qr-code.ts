import {Feature} from "./feature";

export interface QrCode {
    _id?: string;
    name: string;
    features: Feature[];
}