import {Feature} from "./feature";
import {Predictions} from "./prediction";
import {QrCode} from "./qr-code";

export interface Result {
    _id?: string;
    trackId: string;
    fileName: string;
    feature: Feature;
    predictions: Predictions;
    fileSize: number;
    startDate: number;
    endDate: number;
    relatedQrCode?: QrCode;
}