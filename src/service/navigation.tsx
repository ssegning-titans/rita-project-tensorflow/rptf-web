import React from "react";
import {createBrowserRouter} from "react-router-dom";
import {Home} from "../screens/home";
import {SingleQrcode} from "../screens/single-qrcode";
import {Qrcode} from "../screens/qrcode";
import {AllQrcode} from "../screens/all-qrcode";
import {Result} from "../screens/result";
import {AllResult} from "../screens/all-result";
import {Landing} from "../screens/landing";
import {AddQrcode} from "../screens/add-qrcode";
import {File} from "../screens/file";
import {AllFile} from "../screens/all-file";
import {SearchResult} from "../screens/search-results";

export const router = createBrowserRouter([
    {
        path: '/',
        element: <Home/>,
        children: [
            {
                path: '',
                element: <Landing/>,
            },
            {
                path: 'qr-codes',
                element: <Qrcode/>,
                children: [
                    {
                        path: '',
                        element: <AllQrcode/>,
                    },
                    {
                        path: 'add',
                        element: <AddQrcode/>,
                    },
                    {
                        path: ':id',
                        element: <SingleQrcode/>,
                    },
                ]
            },
            {
                path: 'results',
                element: <Result/>,
                children: [
                    {
                        path: 'search',
                        element: <SearchResult/>,
                    },
                    {
                        path: '',
                        element: <AllResult/>,
                    },
                ]
            },
            {
                path: 'files',
                element: <File/>,
                children: [
                    {
                        path: '',
                        element: <AllFile/>,
                    },
                ]
            },
        ]
    },
]);