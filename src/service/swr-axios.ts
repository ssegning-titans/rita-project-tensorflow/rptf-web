import axios, {AxiosError} from 'axios';
import useSWR from 'swr';
import {QrCode} from "../types/qr-code";
import {Result} from "../types/result";
import {useState} from "react";
import {AppFile} from "../types/file";

export const baseUrl = process.env.REACT_APP_BASE_URL ?? '';

const fetcher = (url: string) => axios.get(baseUrl + url).then(res => res.data);

export function useQrCode() {
    const {data, error, isLoading} = useSWR<QrCode[], AxiosError<any>>(() => '/api/v1/qr-code', fetcher);
    return {data, error, isLoading};
}

export function useSingleQrCode(id: string) {
    const {data, error, isLoading} = useSWR<QrCode, AxiosError<any>>(() => `/api/v1/qr-code/${id}`, fetcher);
    return {data, error, isLoading};
}

export function useResults() {
    const {data, error, isLoading} = useSWR<Result[], AxiosError<any>>(() => '/api/v1/results', fetcher);
    return {data, error, isLoading};
}

export function useSingleResults(id: string) {
    const {data, error, isLoading} = useSWR<Result, AxiosError<any>>(() => `/api/v1/results/${id}`, fetcher);
    return {data, error, isLoading};
}

export function useFiles(page = 0, limit = 20) {
    const {
        data,
        error,
        isLoading
    } = useSWR<AppFile[], AxiosError<any>>(() => `/api/v1/files?page=${page}&limit=${limit}`, fetcher);
    return {data, error, isLoading};
}

type AxiosPost<T, E, P = T> = [
    (payload: P) => Promise<T>,
    { data?: T, error?: E, isLoading: boolean }
];

export function useAxiosPost<T = any, E = any, P = T>(url: string): AxiosPost<T, E, P> {
    const [data, setData] = useState<T>();
    const [error, setError] = useState<E>();
    const [isLoading, setLoaded] = useState(false);

    const mutate = async (payload: P) => {
        let res: any = undefined;
        let error: any = undefined;
        try {
            const response = await axios.post(
                url,
                payload
            );

            setData(response.data);
            res = response.data;
        } catch (e: any) {
            setError(e);
            error = e;
        } finally {
            setLoaded(true);
        }

        if (error) {
            throw error;
        }

        return res;
    };

    return [mutate, {data, error, isLoading}];
}

export function useCreateQrCode(): AxiosPost<QrCode, AxiosError<any>> {
    const [mutate, {
        data,
        error,
        isLoading
    }] = useAxiosPost<QrCode, AxiosError<any>>(`${baseUrl}/api/v1/qr-code`);
    return [mutate, {data, error, isLoading}];
}

export function useFindResults<P = {}>(): AxiosPost<Result[], AxiosError<any>, P> {
    const [mutate, {
        data,
        error,
        isLoading
    }] = useAxiosPost<Result[], AxiosError<any>, P>(`${baseUrl}/api/v1/results/search`);
    return [mutate, {data, error, isLoading}];
}