FROM nginx:alpine

ENV ENV 'production'

LABEL maintainer="SSegning DEV <dev@ssegning.com>"

# Set working directory to nginx asset directory
WORKDIR /usr/share/nginx/html

# Remove default nginx static assets
RUN rm -rf ./*

COPY nginx.conf /etc/nginx/conf.d/default.conf

# Copy static assets from builder stage
COPY build .

# Containers run nginx with global directives and daemon off
ENTRYPOINT ["nginx", "-g", "daemon off;"]
